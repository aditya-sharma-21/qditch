from django.db import models
from django.conf import settings
from django.utils import timezone

from users.models import Users, Business

# Create your models here.

class Booking(models.Model):
    user = models.ForeignKey(Users, related_name='bookings', on_delete=models.CASCADE)
    business = models.ForeignKey(Business, related_name='bookings', on_delete=models.CASCADE)
    status = models.BooleanField()
    total_cost = models.PositiveIntegerField()

class BookingServices(models.Model):
    booking = models.ForeignKey(Booking, related_name='booking_services', on_delete=models.CASCADE)
    date_and_time = models.CharField(max_length=6)