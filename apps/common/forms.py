import json
from allauth.account.forms import LoginForm
from rest_framework.authtoken.models import Token
from django.http import JsonResponse
from django.core import serializers
from rest_framework.response import Response
from django.shortcuts import render

class MyCustomLoginForm(LoginForm):

    def login(self, *args, **kwargs):

        token = Token.objects.filter(user = self.user).values_list('key', flat=True)
        data = serializers.serialize('json', Token.objects.filter(user = self.user))

        new_data = json.loads(data)
        print('##################################')
        print(new_data)
        print(new_data[0]['pk'])
        # return Response({
        #     'token': new_data[0]['pk']
        # })
        return JsonResponse({'token': new_data[0]['pk']})