from django.urls import path, re_path
from django.conf.urls import url

urlpatterns += [
    url(r'^api-token-auth/', CustomAuthToken.as_view())
]