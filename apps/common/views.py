import json
from django.shortcuts import render
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
# from rest_framework import serializers
from django.http import JsonResponse
from django.core import serializers
from rest_auth.registration.views import RegisterView, VerifyEmailView
from .forms import MyCustomLoginForm

from users.models import Business

# Create your views here.

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):

        # serializer = self.serializer_class(data=request.data, email = self.email, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        business_id = Business.objects.filter(user = user.pk).values_list('id', flat=True)
        data = serializers.serialize('json', Business.objects.filter(user = user.pk))
        new_data = json.loads(data)
        get_business_id = new_data[0]['pk']
        return Response({
            'token': token.key,
            'user': user.pk,
            'email': user.email,
            'business': get_business_id
        })


# class MyVerifyEmailView(VerifyEmailView):   
#     """
#     View inherited from allauth's verify email view.
#     """
#     allowed_methods = list(VerifyEmailView.allowed_methods) + ['GET']

#     def get(self, request, *args, **kwargs):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         self.kwargs['key'] = serializer.validated_data['key']
#         confirmation = self.get_object()
#         confirmation.confirm(self.request)
#         print('##############################################')
#         res = redirect('templates/account/email_verification.html')
#         return Response({'detail': _('ok')}, status=status.HTTP_200_OK)

    
# class VerifyEmailToken(viewset.View):
#     pass