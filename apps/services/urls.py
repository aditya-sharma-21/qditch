from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from services.views import ServicesViewSet, BusinessServicesViewSet

router = DefaultRouter()

router.register('services', ServicesViewSet, basename='services')
router.register('business_services', BusinessServicesViewSet, basename = 'business_services')

urlpatterns = router.urls