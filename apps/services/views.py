from django.shortcuts import render
from rest_framework import filters, generics, viewsets
from django_filters.rest_framework import DjangoFilterBackend

from services.models import Services, BusinessServices
from services.api.serializer import ServicesSerializer, BusinessServicesSerializer

# Create your views here.

class ServicesViewSet(viewsets.ModelViewSet):
    queryset = Services.objects.all()
    serializer_class = ServicesSerializer
    name = 'services'

class BusinessServicesViewSet(viewsets.ModelViewSet):
    queryset = BusinessServices.objects.all()
    serializer_class = BusinessServicesSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ('service',)
    name = 'business_services'