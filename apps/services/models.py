from django.db import models
from django.conf import settings
from django.utils import timezone

from users.models import Business
from categories.models import Categories

# Create your models here.

class Services(models.Model):
    categories = models.ForeignKey(Categories, related_name='services', on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    # default_price = models.PositiveSmallIntegerField()
    # default_duration = models.CharField(max_length=6)

    def __str__(self):
        return self.name

class BusinessServices(models.Model):
    business = models.ForeignKey(Business, related_name='business_services', on_delete=models.CASCADE)
    service = models.ForeignKey(Services, related_name='business_services', on_delete=models.CASCADE)
    business_service_price = models.PositiveSmallIntegerField()
    business_service_duration = models.CharField(max_length=6)
    buffer_time = models.CharField(max_length=4)
    disable = models.BooleanField()

    def __str__(self):
        return str(self.service.id)
