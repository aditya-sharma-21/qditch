from rest_framework import serializers
from services.models import Services, BusinessServices

class ServicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Services
        fields = ('id', 'categories', 'name')

class BusinessServicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessServices
        fields = ('id', 'business', 'service', 'business_service_price', 'business_service_duration', 'buffer_time', 'disable')