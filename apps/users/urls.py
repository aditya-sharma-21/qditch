from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from users.views import UserViewSet, BusinessViewSet, OTPViewSet, UserDetailsViewSet

router = DefaultRouter()

router.register('user', UserViewSet, basename='user')
router.register('user_details', UserDetailsViewSet, basename = 'user_details')
router.register('business', BusinessViewSet, basename='business')
router.register('otp', OTPViewSet, basename='otp')

urlpatterns = router.urls  