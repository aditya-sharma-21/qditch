from django.db import models

from django.dispatch import receiver
from django.conf import settings
from django.db.models.signals import post_save
from django.db.models.signals import *

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib import auth

from common.models import User

# Create your models here.

class BusinessType(models.TextChoices):
    MALE = 'male', _('Male'),
    FEMALE = 'female', _('Female')
    UNISEX = 'unisex', _('Unisex')

class Gender(models.TextChoices):
        """
        Class for registering gender of the customer.
        """
        
        MALE = 'M', _('Male')
        FEMALE = 'F', _('Female')
        OTHERS = 'O', _('Others')

class Users(User):
    # mobile_number = models.CharField(max_length=10)
    # gender = models.CharField(max_length=1, choices=Gender.choices, blank=True, null=True)
    # role = models.CharField(max_length=1)
    verified = models.BooleanField(null=False, blank=False, default=False)
    # password = models.CharField()
    # created_date = models.DateTimeField(default=timezone.now)
    # updated_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.updated_date = timezone.now()
        self.save()

    def __str__(self):
        return self.email


class UserDetails(models.Model):
    users = models.OneToOneField(Users, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    mobile_number = models.CharField(max_length=10)
    gender = models.CharField(max_length=1, choices=Gender.choices, blank=True, null=True)
    role = models.CharField(max_length=1)

class Business(models.Model):
    user = models.ForeignKey(Users, related_name='business_users', on_delete=models.CASCADE)
    business_name = models.CharField(max_length=30, blank=False, null=False)
    business_type = models.CharField(max_length=7, choices = BusinessType.choices)
    map_url = models.TextField(blank=True, null=True)
    stylist_available = models.PositiveSmallIntegerField(null=False, blank=False)
    def __str__(self):
        return self.business_name

class OTP(models.Model):
    user = models.ForeignKey(Users, related_name='user_otp', on_delete=models.CASCADE)
    value = models.CharField(max_length=10)