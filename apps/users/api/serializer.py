from rest_framework import serializers
from users.models import Users, Business, OTP, UserDetails
from rest_auth.models import TokenModel

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = (
            'id', 'first_name', 'last_name', 
            'email', 'password','gender', 'username',
            'mobile_number', 'verified','role',
            )

    def create(self, validated_data):
        user = Users(
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetails
        fields = '__all__'

class BusinessSerializer(serializers.ModelSerializer):
    business_categories = serializers.StringRelatedField(many=True, read_only=True)
    business_safety_features = serializers.StringRelatedField(many=True, read_only=True)
    business_services = serializers.StringRelatedField(many=True, read_only=True)
    business_images = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    business_stylist = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    business_timings = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    business_location = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = Business
        fields = (
            'id', 'user', 'business_name', 'business_type', 
            'map_url', 'stylist_available', 'business_categories', 
            'business_services','business_safety_features',
            'business_images', 'business_stylist', 'business_timings', 'business_location'
        )

class OTPSerializer(serializers.ModelSerializer):
    class Meta:
        model = OTP
        fields = ('id', 'user', 'value')


class RestAuthSerializer(serializers.ModelSerializer):
    business_users = serializers.PrimaryKeyRelatedField(many=True ,read_only=True)
    class Meta:
        model = Users
        fields = ('id', 'email', 'business_users')