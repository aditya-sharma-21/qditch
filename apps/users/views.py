import json
from django.shortcuts import render
from rest_framework import filters, generics, viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework import filters
from rest_framework.authtoken.models import Token
from django.http import HttpResponse
from django.http import JsonResponse
from django.core import serializers
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.authtoken.views import ObtainAuthToken
from rest_auth.views import LoginView
from users.models import Users, Business, OTP, UserDetails
from users.api.serializer import UsersSerializer, BusinessSerializer, OTPSerializer, UserDetailsSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet, ObtainAuthToken):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer
    name = 'user_list'

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.save()
        token = Token.objects.create(user=user)

        data = serializer.data
        headers = self.get_success_headers(serializer.data)
        return Response({
            'token': token.key,
            'user': user.pk,
            'email': user.email,
        })

class UserDetailsViewSet(viewsets.ModelViewSet):
    queryset = UserDetails.objects.all()
    serializer_class = UserDetailsSerializer
    name = 'user_details_list'

class BusinessViewSet(viewsets.ModelViewSet):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ('business_type', 'stylist_available')
    search_fields = ['business_name', 'business_type']
    name = 'business_list'

class OTPViewSet(viewsets.ModelViewSet):
    queryset = OTP.objects.all()
    serializer_class = OTPSerializer
    name = 'otp_list'

class CustomLoginView(LoginView):
    def get_response(self):
        orginal_response = super().get_response()
        user = Users.objects.filter(email = self.request.data['email'])
        json_user = serializers.serialize('json', Users.objects.filter(email = self.request.data['email']))
        new_user = json.loads(json_user)
        business = Business.objects.filter(user = new_user[0]['pk'])
        if business.exists():
            json_business = serializers.serialize('json', business)
            new_business = json.loads(json_business)
            mydata = {"user": new_user[0]['pk'], "business": new_business[0]['pk']}
        else:
            mydata = {"user": new_user[0]['pk'], "business": None}
        orginal_response.data.update(mydata)
        return orginal_response