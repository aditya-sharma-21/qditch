from django.apps import AppConfig


class SafetyFeaturesConfig(AppConfig):
    name = 'safety_features'
