from django.shortcuts import render
from rest_framework import filters, generics, viewsets

from safety_features.models import SafetyFeature, BusinessSafetyFeature
from safety_features.api.serializer import SafetyFeatureSerailizer, BusinessSafetyFeatureSerializer

# Create your views here.

class SafetyFeatureViewSet(viewsets.ModelViewSet):
    queryset = SafetyFeature.objects.all()
    serializer_class = SafetyFeatureSerailizer
    name = 'safety_feature_list'

class BusinessSafetyFeatureViewSet(viewsets.ModelViewSet):
    queryset = BusinessSafetyFeature.objects.all()
    serializer_class = BusinessSafetyFeatureSerializer
    name = 'business_safety_feature_list'