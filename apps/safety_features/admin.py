from django.contrib import admin

from safety_features.models import SafetyFeature

# Register your models here.

admin.site.register(SafetyFeature)