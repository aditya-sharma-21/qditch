from django.db import models

from users.models import Business

# Create your models here.

class SafetyFeature(models.Model):
    safety_feature = models.TextField()
    def __str__(self):
        return self.safety_feature


class BusinessSafetyFeature(models.Model):
    business = models.ForeignKey(Business, related_name='business_safety_features', on_delete=models.CASCADE)
    safety_features = models.ForeignKey(SafetyFeature, related_name='business_safety_features', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.safety_features.id)