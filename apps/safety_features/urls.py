from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from safety_features.views import SafetyFeatureViewSet, BusinessSafetyFeatureViewSet

router = DefaultRouter()

router.register('safety_features', SafetyFeatureViewSet, basename='safety_features')
router.register('business_safety_features', BusinessSafetyFeatureViewSet, basename = 'business_safety_features')

urlpatterns = router.urls