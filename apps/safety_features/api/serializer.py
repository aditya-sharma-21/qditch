from rest_framework import serializers

from safety_features.models import SafetyFeature, BusinessSafetyFeature

class SafetyFeatureSerailizer(serializers.ModelSerializer):
    class Meta:
        model = SafetyFeature
        fields = ('id', 'safety_feature')

class BusinessSafetyFeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessSafetyFeature
        fields = ('id', 'business', 'safety_features')