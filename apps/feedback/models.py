from django.db import models
from django.conf import settings
from django.utils import timezone

from users.models import Users, Business

# Create your models here.

class Feedback(models.Model):
    user = models.ForeignKey(Users, related_name='feedbacks', on_delete=models.CASCADE)
    business = models.ForeignKey(Business, related_name='feedbacks', on_delete=models.CASCADE)
    rating = models.PositiveIntegerField()
    review = models.CharField(max_length=150)