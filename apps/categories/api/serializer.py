from rest_framework import serializers
from categories.models import Categories, BusinessCategories

class CategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = ('id','name',)

class BusinessCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessCategories
        fields = ('id', 'business', 'category', 'super_category')

class BusinessSuperCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessCategories
        fields = ('id', 'business', 'category')