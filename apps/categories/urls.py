from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from categories.views import CategoriesViewSet, BusinessCategoriesViewSet, BusinessSuperCategoryViewSet

router = DefaultRouter()

router.register('categories', CategoriesViewSet, basename = 'categories')
router.register('business_categories', BusinessCategoriesViewSet, basename = 'business_categories')
router.register('business_super_categories', BusinessSuperCategoryViewSet ,basename = 'business_super_categories')

urlpatterns = router.urls