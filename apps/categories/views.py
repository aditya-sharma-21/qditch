from django.shortcuts import render
from rest_framework import filters, generics, viewsets

from categories.models import Categories, BusinessCategories
from categories.api.serializer import CategoriesSerializer, BusinessCategoriesSerializer, BusinessSuperCategorySerializer
from rest_framework import permissions

# Create your views here.

class CategoriesViewSet(viewsets.ModelViewSet):
    queryset = Categories.objects.all()
    serializer_class = CategoriesSerializer
    name = 'categories'
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class BusinessCategoriesViewSet(viewsets.ModelViewSet):
    queryset = BusinessCategories.objects.all()
    serializer_class = BusinessCategoriesSerializer
    filterset_fields = ('category',)
    name = 'business_categories'

class BusinessSuperCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = BusinessCategories.objects.filter(super_category = True)
    serializer_class = BusinessSuperCategorySerializer
    name = 'business_super_categories'