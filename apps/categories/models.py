from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from users.models import Business

# Create your models here.

class Categories(models.Model):
    name = models.CharField(max_length=20, unique=True)
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.updated_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name

class BusinessCategories(models.Model):
    business = models.ForeignKey(Business, related_name='business_categories', on_delete=models.CASCADE)
    category = models.ForeignKey(Categories, related_name='categories', on_delete=models.CASCADE)
    super_category = models.BooleanField()
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.category.id)

    def publish(self):
        self.updated_date = timezone.now()
        self.save()