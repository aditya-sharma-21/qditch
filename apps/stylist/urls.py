from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from stylist.views import StylistViewSet

router = DefaultRouter()

router.register('stylist_details', StylistViewSet, 'stylist')

urlpatterns = router.urls