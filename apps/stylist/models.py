from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from users.models import Business
# Create your models here.

class StylistStatus(models.TextChoices):
    AVAILABLE = 'A', _('Available')
    BUSY = 'B', _('Busy')
    LEAVE = 'L', _('Leave')

class Stylist(models.Model):
    business = models.ForeignKey(Business, related_name='business_stylist', on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    status = models.CharField(max_length=1, choices = StylistStatus.choices, default=StylistStatus.AVAILABLE)
    created_date = models.DateTimeField(default=timezone.now)