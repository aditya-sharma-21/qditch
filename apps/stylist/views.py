import json
from django.shortcuts import render
from rest_framework import filters, generics, viewsets
from rest_framework.response import Response
from rest_framework import status
from django.core import serializers

from stylist.models import Stylist
from stylist.api.serializer import StylistSerializer
from users.models import Business

# Create your views here.

class StylistViewSet(viewsets.ModelViewSet):
    queryset = Stylist.objects.all()
    serializer_class = StylistSerializer
    name = 'stylist'

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data = request.data, many = isinstance(request.data, list))
        business = request.data
        get_business_id = business['business']
        business_user = serializers.serialize('json', Business.objects.filter(id = get_business_id))
        json_business_user = json.loads(business_user)
        add_new_stylist = json_business_user[0]['fields']['stylist_available'] + 1
        new_business_model = Business.objects.filter(id = get_business_id).update(stylist_available = add_new_stylist)
        serializer.is_valid(raise_exception = True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)