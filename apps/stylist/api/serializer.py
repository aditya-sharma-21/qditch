from rest_framework import serializers
from stylist.models import Stylist

class StylistSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Stylist
        fields = ('id', 'business','name')
