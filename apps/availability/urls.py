from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from availability.views import TimingViewSet, LocationViewSet

router = DefaultRouter()

router.register('timing', TimingViewSet, basename='timing')
router.register('location', LocationViewSet, basename='location')

urlpatterns = router.urls