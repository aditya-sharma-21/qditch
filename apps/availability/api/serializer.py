from rest_framework import serializers
from availability.models import Timing, Location

class TimingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timing
        fields = ('id', 'business', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'business', 'lattitude', 'longitute')