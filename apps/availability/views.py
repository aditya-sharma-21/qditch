from django.shortcuts import render
from rest_framework import filters, generics, viewsets

from availability.models import Timing, Location
from availability.api.serializer import TimingSerializer, LocationSerializer

# Create your views here.

class TimingViewSet(viewsets.ModelViewSet):
    queryset = Timing.objects.all()
    serializer_class = TimingSerializer
    name = 'timming'

class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    name = 'location'