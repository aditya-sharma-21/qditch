from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from users.models import Business

# Create your models here.


class Timing(models.Model):
    business = models.ForeignKey(Business, related_name='business_timings', on_delete=models.CASCADE)
    monday = models.CharField(max_length=15)
    tuesday = models.CharField(max_length=15)
    wednesday = models.CharField(max_length=15)
    thursday = models.CharField(max_length=15)
    friday = models.CharField(max_length=15)
    saturday = models.CharField(max_length=15)
    sunday = models.CharField(max_length=15)

class Location(models.Model):
    business = models.ForeignKey(Business, related_name='business_location', on_delete=models.CASCADE)
    lattitude = models.IntegerField()
    longitute = models.IntegerField()