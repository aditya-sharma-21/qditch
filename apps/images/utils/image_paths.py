def get_profile_image_path(obj, filename):
    return f'images/profiles/{filename}'

def get_business_image_path(obj, filename):
    return f'images/business_images/{filename}'