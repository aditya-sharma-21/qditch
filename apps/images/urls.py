from django.urls import path, re_path
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns

from images.views import ProfileImageViewSet, BusinessImageViewSet, BusinessCoverImageViewSet

router = DefaultRouter()

router.register('profile_image', ProfileImageViewSet, basename='profile_image')
router.register('business_image', BusinessImageViewSet, basename = 'business_image')
router.register('business_cover_image', BusinessCoverImageViewSet, basename = 'business_cover_image')

urlpatterns = router.urls