from rest_framework import serializers
from images.models import ProfileImage, BusinessImage

class ProfileImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileImage
        fields = ('id', 'user', 'blob_data')

class BusinessImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessImage
        fields = ('id', 'business', 'cover', 'blob_data')
