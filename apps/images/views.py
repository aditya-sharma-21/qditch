from django.shortcuts import render
from rest_framework import filters, generics, viewsets

from images.models import ProfileImage, BusinessImage
from images.api.serializer import ProfileImageSerializer, BusinessImageSerializer
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
# Create your views here.

class ProfileImageViewSet(viewsets.ModelViewSet):
    queryset = ProfileImage.objects.all()
    serializer_class = ProfileImageSerializer
    parser_classes = (MultiPartParser,FormParser,JSONParser)
    name = 'profile_image'

class BusinessImageViewSet(viewsets.ModelViewSet):
    queryset = BusinessImage.objects.all()
    serializer_class = BusinessImageSerializer
    parser_classes = (MultiPartParser,FormParser,JSONParser)
    name = 'business_images'

class BusinessCoverImageViewSet(viewsets.ModelViewSet):
    queryset = BusinessImage.objects.filter(cover = True)
    serializer_class = BusinessImageSerializer
    name = 'business_cover_image'