from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from images.utils.image_paths import get_profile_image_path, get_business_image_path

from users.models import Users, Business

# Create your models here.


class ProfileImage(models.Model):
    user = models.OneToOneField(Users, on_delete=models.CASCADE)
    blob_data = models.ImageField(upload_to=get_profile_image_path, blank=True, null=True)

    # def profile_tag(self):
    #     if self.profile:
    #         url = self.profile.url
    #         return mark_safe(f'<img src={url} height="100px" width="150px" />')

class BusinessImage(models.Model):
    business = models.ForeignKey(Business, related_name='business_images', on_delete=models.CASCADE)
    cover = models.BooleanField(default=False)
    blob_data = models.ImageField(upload_to=get_business_image_path, blank=True, null=True)