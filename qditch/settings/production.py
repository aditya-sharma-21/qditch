from qditch.settings.base_settings import *

DEBUG = False

# SECRET_KEY = os.environ['SECRET_KEY']


# SECURITY WARNING: update this when you have the production host
ALLOWED_HOSTS = ['*']

#STATIC SETTINGS:
STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

'''MEDIA SETTINGS:
MEDIA_ROOT = os.path.join(BASE_DIR,'images')
MEDIA_URL = '/images/' '''
