"""qditch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import permissions
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework_swagger.views import get_swagger_view
from rest_framework.authtoken import views 
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt
from allauth.account.views import LoginView
from users.views import CustomLoginView
# from common.views import NameRegistrationView

# from common.views import , VerifyEmailView

swagger_schema_view = get_swagger_view(title='QditchBackend EndPoints')

schema_view = get_schema_view(
    openapi.Info(
        title="Qditch API EndPoints",
        default_version='v1',
        description="These are all the APIs for the qditch company.",
    ),
    public=True,
    # permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/users/', include('users.urls')),
    path('api/images/', include('images.urls')),
    path('api/availability/', include('availability.urls')),
    path('api/stylist/', include('stylist.urls')),
    path('api/service/', include('services.urls')),
    path('api/category/', include('categories.urls')),
    path('api/safety_feature/', include('safety_features.urls')),
    path('accounts/', include('allauth.urls')),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'custom/login/', CustomLoginView.as_view(), name='my_custom_login')
    # url(r'^rest-auth/registration/', NameRegistrationView.as_view(), name="rest_name_register")
]

if settings.DEBUG:
    # import debug_toolbar
    # urlpatterns = [
    #     path('__debug__/', include(debug_toolbar.urls)),
    # ] + urlpatterns
    
    urlpatterns += static(settings.STATIC_URL,
                document_root=settings.STATIC_ROOT)


if settings.DEBUG:        
    urlpatterns += static(settings.MEDIA_URL,
                        document_root=settings.MEDIA_ROOT)